<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use DB;
use Validator;
use Session;
use Redirect;
use Hash;
use Auth;
use Illuminate\Routing\Route;

 error_reporting(0);

 class TestController extends Controller
 {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo 'Hello index controller';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       echo 'Hello create controller';
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = $request->all();
        if($post){
            if($post['types'] == 1){
                $rules = array('model' => 'required|max:100', 
                               'color' => 'required|max:20'); 
            }else if($post['types'] == 2){
                $rules = array('name' => 'required|max:40', 
                               'gender' => 'required'); 
            }
           
            $validator = Validator::make($post, $rules);
            if ($validator->fails()) {
             $messages = $validator->messages();
             Session::flash('error', $message); 
             return Redirect::to('notification')->withErrors($validator)->withInput();
            }else{
                $sql = DB::table('users')
                                ->select('*')
                                ->get();
                if(!empty($sql)){
                    foreach($sql as $val){
                        if(!empty($val->token)){
                            $st = $this->androidPush($post,$val->token); 
                            $mt[] = $st; 
                        }
                    }
                Session::flash('success', 'Push notification send successfully.'); 
                }
            }
        }
        return view('welcome'); 
    }


/**
* send push notification
*/
private function androidPush($post, $token){

        $types = $post['types'];
        if($types == 1){
            $types = 'cars';
            $model = $post['model'];
            $color = $post['color'];
            $notification = array('types' =>$types, 'model' =>$model , 'color' => $color,
                'title' =>$types , 'body' => $model.' '.$color);
        }else if($types == 2){
            $types = 'people';
            $name = $post['name'];
            $gender = $post['gender'];
            $notification = array('types' =>$types, 'name' =>$name , 'gender' => $gender, 'title' =>$types , 'body' => $name.' '.$gender);
        }
         
         
        $ch = curl_init("https://fcm.googleapis.com/fcm/send");

        $arrayToSend = array('to' => $token, 'notification' => $notification,'data' => $notification,'priority'=>'high');
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
       $headers[] = 'Authorization: key=AAAAcyqybEc:APA91bE-1wsJfW6JauvPvjOBOKkPS7U1RKr_yIBpOLni7z8R-G-fpMplqIBJplWx93ejPB_SOIOuvAWDrLjxy8FVmNVfsu20_9U6wxpxRjGkvBrGNWyKrcuLF_QoqBjsg_eWbTcmMuzW'; //
       curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                
       curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
       curl_setopt($ch, CURLOPT_HTTPHEADER,$headers); 
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);    
       $response = curl_exec($ch);
       curl_close($ch);
       return $response;
       return true;
}

/**
* registration
*/
public function registration(Request $request)
{
    $post = $request->all();
    if($post){

        $rules = array('deviceType' => 'required|max:10', 
                        'token' => 'required');
        $validator = Validator::make($post, $rules);
        if ($validator->fails()) {
         $messages = $validator->messages();
         $message = array('status' => 0, 'message' => $messages); 
        }else{
            $where = array('token' => $post['token']);
            $sql = DB::table('users')
                            ->select('*')
                            ->where($where)
                            ->get();
            if(empty($sql)){
                $data = array('deviceType' => $post['deviceType'],
                          'token' => $post['token']);
                $insertId =  DB::table('users')->insertGetId($data);
                $message = array('status' => 1, 'message' => 'successfully register.'); 
            }else{
                $message = array('status' => 1, 'message' => 'Already register.'); 
            }
        }
    
    }else{
      $message = array('status' => 0, 'message' => 'Please send require parameter.');   
    }
    return $message;
}



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
