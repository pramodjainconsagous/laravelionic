
function submitform()
{
  $('#submitbtn').trigger( "click" );
  
} 
var ulr1 = $("#ajax_url1").val();
var ulr2 = $("#ajax_url2").val();
var path = $("#upload_path").val(); 
var path1 = "/public/assets/images/loading.gif ";
var loading_img = "<img src="+path1+"/>";
$(document).ready(function() {
		$('.close').click(function() { 
		$('#messageviewimage').html('');
		$('.imgareaselect-outer').css( "display", "none" );
		$( ".imgareaselect-selection" ).parent('div').css( "display", "none" );
		});   

        
		$("#upload_form").on('submit',(function(e){
				e.preventDefault();
				$("#viewimage").html('Please wait...'+loading_img);
					$.ajax({
						url: ulr1,
						type: "POST",
						data:  new FormData(this),
						contentType: false,
						cache: false,
						processData:false,
						success: function(data){
							
							// alert(data);
								$("#viewimage").html('');
							showResponse(data); 
							//location.reload();
							

						},
						error: function(){} 	        
					});
				}));	
            // $("#upload_form").ajaxForm({
            // 	url: 'upload',
            //     success:    showResponse 
            // }).submit();
       
//thumbnail upload
 	$("#thumbnail_form").on('submit',(function(e){
				e.preventDefault();
					$.ajax({
						url: ulr2,
						type: "POST",
						data:  new FormData(this),
						contentType: false,
						cache: false,
						processData:false,
						success: function(data){
							
							$("#temp_img").html('<img src="'+path+'/thumb_'+data+'" style="width:150px;" />');
							$("#tempImg").css( "display", "none" );
							$("#pre_saved").css( "display", "none" );
							
							$('.close').click();
							$("#viewimage").html('');
							$("#thumbviewimage").html('');
							 
							
							
							

						},
						error: function(){} 	        
					});
				}));

$('#save_thumb').click(function() {
var x1 = $('#x1').val();
var y1 = $('#y1').val();
var x2 = $('#x2').val();
var y2 = $('#y2').val();
var w = $('#w').val();
var h = $('#h').val();
if(x1=="" || y1=="" || x2=="" || y2=="" || w=="" || h==""){
alert("Please Make a Selection First");
return false;
}else{
return true;
}
});

    });
    
    function showResponse(responseText, statusText, xhr, $form){

	    if(responseText.indexOf('.')>0){ 
	    	
			$('#thumbviewimage').html('<img src="'+path+'/'+responseText+'" id="lazy" style="position: relative;" alt="Thumbnail Preview" />');
			$('#viewimage').load();
			$('#viewimage').html('Please wait while loading.');
	    	$('#viewimage').html('<img class="preview" alt="" src="'+path+'/'+responseText+'"   id="thumbnail" />');
	    	
	    	$('#filename').val(responseText); 
			$('#thumbnail').imgAreaSelect({  aspectRatio: '1:1', handles: true  , onSelectChange: preview, x1: 120, y1: 90, x2: 280, y2: 210 });
		}else{
			$('#thumbviewimage').html(responseText);
	    	$('#viewimage').html(responseText);
		}
    }
function preview(img, selection) { 
	var scaleX = 150 / selection.width; 
	var scaleY = 150 / selection.height; 

	$('#thumbviewimage > img').css({
		width: Math.round(scaleX * img.width) + 'px', 
		height: Math.round(scaleY * img.height) + 'px',
		marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px', 
		marginTop: '-' + Math.round(scaleY * selection.y1) + 'px' 
	});
	
	var x1 = Math.round((img.naturalWidth/img.width)*selection.x1);
	var y1 = Math.round((img.naturalHeight/img.height)*selection.y1);
	var x2 = Math.round(x1+selection.width);
	var y2 = Math.round(y1+selection.height);
	
	$('#x1').val(x1);
	$('#y1').val(y1);
	$('#x2').val(x2);
	$('#y2').val(y2);	
	
	$('#w').val(Math.round((img.naturalWidth/img.width)*selection.width));
	$('#h').val(Math.round((img.naturalHeight/img.height)*selection.height));
	
} 
