function data_table(table_name = 'example1'){
	$('#'+table_name).dataTable({
		language: {
       processing:     "lopende behandeling...",
       search:         "Rechercher&nbsp;:",
       lengthMenu:    "Tonen _MENU_ Regels",
       info:           "Tonen _START_ tot _END_ van _TOTAL_ Resultaten",
       infoEmpty:      "Geen resultaten om weer te geven",
       infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
       infoPostFix:    "",
       loadingRecords: "Een moment geduld aub - bezig met laden......",
       zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
       emptyTable:     "Geen resultaten aanwezig in de tabel",
       paginate: {
           first:      "Eerste",
           previous:   "Laatste",
           next:       "Volgende",
           last:       "Vorige"
       },
       aria: {
           sortAscending:  ": activeer om kolom oplopend te sorteren",
           sortDescending: ": activeer om kolom aflopend te sorteren"
       },
       search: "_INPUT_",
       searchPlaceholder: "Zoeken..."
   },
		responsive: true,
		pageLength: 10,
	
  });
}