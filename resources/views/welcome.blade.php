@extends('layouts.app')
@section('content') 
<h2>Send push notification</h2>
<p id="message"></p>
<form id="pushNotificationForm" method="post" action="{{url('notification')}}">
  <div class="row">
    <div class="col-sm-12">
      <div class="form-group" id="types" name="types">
        <label class="row">Category types:</label>
        <select class="form-control required" name="types" id="types" onchange="selType(this.value);">
         <option value="1">Car</option>
         <option value="2">Person info</option>
       </select>
     </div>
   </div>
 </div>
 <!-- 1 -->
 <div class="row" id="type_1">
  <div class="col-sm-12">
    <div class="form-group">
      <label class="row">Model:</label>
      <input type="text" class="form-control required" id="model" name="model">
    </div>
  </div>

  <div class="col-sm-12">
    <div class="form-group">
      <label class="row">Color:</label>
      <input type="text" class="form-control required" id="color" name="color">
    </div>
  </div>
</div>
<!-- 1 end -->

<!-- 2  -->
<div class="row" id="type_2" style="display: none;">
  <div class="col-sm-12">
    <div class="form-group">
      <label class="row">Name:</label>
      <input type="text" class="form-control required" id="name" name="name">
    </div>
  </div>

  <div class="col-sm-12">
    <label class="row">Gender:</label>
    <div class="form-group"> 
      <div class="col-sm-3">
        <label class="radio-inline">
          <input name="gender" class="required" id="input-gender-male" value="Male" type="radio" />Male
        </label>
      </div>

      <div class="col-sm-3">
        <label class="radio-inline">
          <input name="gender" class="required" id="input-gender-female" value="Female" type="radio" />Female
        </label>
      </div>
    </div>
  </div>
</div>
<!-- 2 end -->

<div class="col-sm-12"><p></p></div>
<div class="col-sm-3">
  <div class="form-group">
    <input type="submit" class="form-control btn btn-primary" id="submit" name="submit" value="Send">
  </div>
</div>


</form>
</div>


@push('js') 
<script type="text/javascript">

  $().ready(function() {
        // validate the comment form when it is submitted
        $("#pushNotificationForm").validate();
      });
  var selType = function(id){
    $('#type_1').hide();
    $('#type_2').hide();
    if(id){
      $('#type_'+id).show();
    }
  }

</script>
@endpush
@push('style') 

<style type="text/css">
.error{color:#800000;}
</style>
@endpush
@endsection
