<!DOCTYPE html>
<html>
<head>
  <title>
    <?=@$title ? @$title : ''?> {{config('app.site_name')}}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{url('assets/vendors/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/vendors/font-awesome/css/font-awesome.min.css')}}">

    <!-- Related css to this page -->
    @stack('css')

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css"> 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>

    <div id="container" class="container-fluid skin-3"> 
      <!-- sidebar menu --> 
      @include('layouts/header') 
      <!-- /end #sidebar --> 
      <!-- main content  -->
      <div class="container">
       @include('layouts/message') 
       @yield('content') 
     </div>
     <!-- ./end #main  --> 
     <!-- footer --> 
     @include('layouts/footer') 
     <!-- /footer --> 
     <!-- end #container --> 
     <!-- Related JavaScript Library to This Pagee --> 
     @stack('js') 
     <!-- Plugins Script --> 
     @stack('style')
     <!-- Plugins Script --> 
     @stack('script')
   </body>
   </html>